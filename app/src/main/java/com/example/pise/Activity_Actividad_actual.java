package com.example.pise;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTabHost;

public class Activity_Actividad_actual extends FragmentActivity {
    public FragmentTabHost tabHost;
    private Intent i;
    private Bundle bundle;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_actual);



        i = this.getIntent();
        bundle = i.getExtras();
        usuario = new Usuario();
        usuario = (Usuario) bundle.getSerializable("usuario");

        Bundle args = new Bundle();

        args.putInt("id",usuario.getId());

        tabHost = findViewById(android.R.id.tabhost);
        tabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        tabHost.addTab(tabHost.newTabSpec("tab_laboral").setIndicator("Empleo"), tab_Laboral.class, args);
        tabHost.addTab(tabHost.newTabSpec("tab_posgrado").setIndicator("Posgrado"), tab_Posgrado.class, args);

        for (int i = 0; i < tabHost.getTabWidget().getChildCount(); i++) {
            TextView tv = (TextView)tabHost.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(Color.rgb(255,255,255));
        }
    }
}
