package com.example.pise;

public class Curso {
    private String nombre;
    private String donde;
    private int id;

    public Curso (){
        this.setNombre("");
        this.setDonde("");
        this.setId(-1);
    }

    public Curso (String nombre, String donde, int id){
        this.setNombre(nombre);
        this.setDonde(donde);
        this.setId(id);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDonde() {
        return donde;
    }

    public void setDonde(String donde) {
        this.donde = donde;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
