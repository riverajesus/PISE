package com.example.pise;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;


public class tab_ListaCursos extends Fragment {

    ListView listaCursos;
    ArrayList<Curso> cursos;
    private tab_ListaCursos.MyArrayAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_lista_cursos, container, false);
        listaCursos = rootView.findViewById(R.id.lvVacantes);
        return rootView;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View v = getView();
        listaCursos = v.findViewById(R.id.lvCursos);

        cursos = new ArrayList<>();
        Curso cur = new Curso("La sociedad mala","CINVESTAV", 1);
        cursos.add(cur);
        cur = new Curso("Inteligencia artificial","CINVESTAV", 2);
        cursos.add(cur);

        adapter = new MyArrayAdapter(getContext(),R.layout.layout_curso,cursos);
        listaCursos.setAdapter(adapter);
    }


    class MyArrayAdapter extends ArrayAdapter<Curso> {
        Context context;
        int textViewRecursoId;
        ArrayList<Curso> objects;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Curso> objects) {
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewRecursoId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View convertView, ViewGroup viewGroup) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewRecursoId, null);

            TextView lblNombre = (TextView) view.findViewById(R.id.txtNombre);
            TextView lblTelefono = (TextView) view.findViewById(R.id.txtNombre2);

            lblNombre.setText(objects.get(position).getNombre());
            lblTelefono.setText(objects.get(position).getDonde());
            return view;
        }

    }

}
