package com.example.pise;

import java.io.Serializable;
import java.util.Date;

public class Usuario implements Serializable {
    private String nombre;
    private String contraseña;
    private int id;
    private String telefono;
    private String celular;
    private String email;
    private String emailAlterno;
    private String carrera;
    private String fotoUrl;
    private String fechaNac;
    private String sexo;
    private String trabajo;
    private String puesto;
    private String fecInicio;
    private String nombreEscuelaPosgrado;
    private String nombrePosgrado;
    private int deseoPosgrado;



    public Usuario(){
        this.setContraseña("");
        this.setId(-1);
        this.setNombre("");
        this.setCarrera("");
        this.setCelular("");
        this.setEmail("");
        this.setEmailAlterno("");
        this.setFechaNac("");
        this.setFotoUrl("");
        this.setSexo("");
        this.setTelefono("");
        this.setTrabajo("");
        this.setPuesto("");
        this.setFecInicio("");
        this.setNombreEscuelaPosgrado("");
        this.setNombrePosgrado("");
        this.setDeseoPosgrado(0);
    }

    public Usuario(String contraseña, String nombre, int id, String carrera, String celular,
                   String email, String emailAlterno, String fechaNac, String fotoUrl,
                   String sexo, String telefono, String trabajo, String puesto,
                   String fecInicio, String nombreEscuelaPosgrado, String nombrePosgrado,
                   int deseoPosgrado){
        this.setNombre(nombre);
        this.setId(id);
        this.setContraseña(contraseña);
        this.setCarrera(carrera);
        this.setCelular(celular);
        this.setEmail(email);
        this.setEmailAlterno(emailAlterno);
        this.setFechaNac(fechaNac);
        this.setFotoUrl(fotoUrl);
        this.setSexo(sexo);
        this.setTelefono(telefono);
        this.setTrabajo(trabajo);
        this.setPuesto(puesto);
        this.setFecInicio(fecInicio);
        this.setDeseoPosgrado(deseoPosgrado);
        this.setNombrePosgrado(nombrePosgrado);
        this.setNombreEscuelaPosgrado(nombreEscuelaPosgrado);
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailAlterno() {
        return emailAlterno;
    }

    public void setEmailAlterno(String emailAlterno) {
        this.emailAlterno = emailAlterno;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getFotoUrl() {
        return fotoUrl;
    }

    public void setFotoUrl(String fotoUrl) {
        this.fotoUrl = fotoUrl;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(String trabajo) {
        this.trabajo = trabajo;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getFecInicio() {
        return fecInicio;
    }

    public void setFecInicio(String fecInicio) {
        this.fecInicio = fecInicio;
    }

    public String getNombreEscuelaPosgrado() {
        return nombreEscuelaPosgrado;
    }

    public void setNombreEscuelaPosgrado(String nombreEscuelaPosgrado) {
        this.nombreEscuelaPosgrado = nombreEscuelaPosgrado;
    }

    public String getNombrePosgrado() {
        return nombrePosgrado;
    }

    public void setNombrePosgrado(String nombrePosgrado) {
        this.nombrePosgrado = nombrePosgrado;
    }

    public int getDeseoPosgrado() {
        return deseoPosgrado;
    }

    public void setDeseoPosgrado(int deseoPosgrado) {
        this.deseoPosgrado = deseoPosgrado;
    }
}
