package com.example.pise;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTabHost;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import DB.ProcesosPHP;
public class tab_Laboral extends Fragment implements Response.Listener<JSONObject>, Response.ErrorListener{

    public FragmentTabHost tabHost;
    private TextView lblPuesto;
    private TextView lblNombreEm;
    private TextView lblEmpresa;
    private EditText edtEmpleoAct;
    private EditText edtEmpresa;
    private EditText edtFecha;
    private Button btnIngresar;
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private String serverip = "https://pise-website-1.000webhostapp.com/WebServices/";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tab_laboral, container, false);
        request = Volley.newRequestQueue(view.getContext());
        lblPuesto = (TextView) view.findViewById(R.id.lblPuesto);
        lblEmpresa = (TextView) view.findViewById(R.id.lblEmpresa);
        lblNombreEm = (TextView) view.findViewById(R.id.lblNombreEm);
        edtEmpleoAct = (EditText) view.findViewById(R.id.edtEmpleoAct);
        edtEmpresa = (EditText) view.findViewById(R.id.edtEmpresa);
        edtFecha = (EditText) view.findViewById(R.id.edtFecha);
        btnIngresar = (Button) view.findViewById(R.id.btnIngresar);

        int id = getArguments().getInt("id");

        final ProcesosPHP procesosPHP = new ProcesosPHP();
        procesosPHP.setContext(view.getContext());
        final Usuario user = new Usuario();
        user.setId(id);
        wsConsultarLaboral(user);



        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (edtEmpleoAct.getText().toString().trim().isEmpty() || edtEmpresa.getText().toString().trim().isEmpty() ||
                        edtFecha.getText().toString().trim().isEmpty()){
                    Toast.makeText(view.getContext(), "Todos los campos deben estar llenos y sin espacios en blanco", Toast.LENGTH_SHORT).show();
                    limpiar();
                }else {
                    user.setTrabajo(edtEmpleoAct.getText().toString());
                    user.setPuesto(edtEmpresa.getText().toString());
                    user.setFecInicio(edtFecha.getText().toString());

                    procesosPHP.wsModificarLaboral(user);
                    Toast.makeText(view.getContext(), "Datos modificados", Toast.LENGTH_SHORT).show();
                    limpiar();
                    wsConsultarLaboral(user);
                }
            }
        });

        return view;
    }


    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {

        Usuario usuario = null;
        final JSONArray json = response.optJSONArray("Trabajos");
        try {
            for (int i=0;i<json.length();i++){
                usuario = new Usuario();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                lblEmpresa.setText(jsonObject.optString("nombre_trabajo"));
                lblPuesto.setText(jsonObject.optString("puesto"));
                lblNombreEm.setText(jsonObject.optString("nombre")+" "+jsonObject.optString("p_paterno")+" "+jsonObject.optString("p_materno"));
            }

        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void wsConsultarLaboral(Usuario c) {
        String url = serverip + "wsConsultarLaboral.php?idUsuario=" + c.getId();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public  void  limpiar()
    {
        edtEmpresa.setText("");
        edtEmpleoAct.setText("");
        edtFecha.setText("");

    }
}