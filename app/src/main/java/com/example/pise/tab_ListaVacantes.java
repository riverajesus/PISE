package com.example.pise;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class tab_ListaVacantes extends Fragment {

    ListView lista;

    ArrayList<Vacante> vacantes;
    private MyArrayAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_lista_vacantes, container, false);
        lista = rootView.findViewById(R.id.lvVacantes);
        return rootView;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        View v = getView();
        lista = v.findViewById(R.id.lvVacantes);

        vacantes = new ArrayList<>();
        Vacante vac = new Vacante("Programador","Solemti", 1);
        vacantes.add(vac);
        vac = new Vacante("Tester","Neoris", 2);
        vacantes.add(vac);

        adapter = new MyArrayAdapter(getContext(),R.layout.layout_vacante,vacantes);
        lista.setAdapter(adapter);
    }

    class MyArrayAdapter extends ArrayAdapter<Vacante> {
        Context context;
        int textViewRecursoId;
        ArrayList<Vacante> objects;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Vacante> objects) {
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewRecursoId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View convertView, ViewGroup viewGroup) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewRecursoId, null);

            TextView lblNombre = (TextView) view.findViewById(R.id.txtNombre);
            TextView lblTelefono = (TextView) view.findViewById(R.id.txtEmpresa);

            lblNombre.setText(objects.get(position).getNombre());
            lblTelefono.setText(objects.get(position).getDonde());
            return view;
        }

    }
}
