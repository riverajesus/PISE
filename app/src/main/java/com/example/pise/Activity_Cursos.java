package com.example.pise;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTabHost;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import DB.ProcesosPHP;

public class Activity_Cursos extends FragmentActivity implements Response.Listener<JSONObject>, Response.ErrorListener{

    ListView listaCursos;
    ArrayList<Curso> cursos;

    private JsonObjectRequest jsonObjectRequest;
    private Activity_Cursos.MyArrayAdapter adapter;
    private RequestQueue request;
    private String serverip = "https://pise-website-1.000webhostapp.com/WebServices/";


    private TextView txtNombre;
    private TextView txtImpartida;
    private TextView txtDia;
    private Button btnEnviar;
    private RadioButton rdbSi;
    private RadioButton rdbNo;

    TabHost tabs;
    public String enviado;
    public int id=0;

    private Intent i;
    private Bundle bundle;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cursos);

        i = this.getIntent();
        bundle = i.getExtras();
        usuario = new Usuario();
        usuario = (Usuario) bundle.getSerializable("usuario");

        listaCursos = findViewById(R.id.lvCursos);
        request = Volley.newRequestQueue(Activity_Cursos.this);

        txtNombre = (TextView) findViewById(R.id.txtNombre);
        txtImpartida = (TextView) findViewById(R.id.txtImpartida);
        txtDia = (TextView) findViewById(R.id.txtDia);
        btnEnviar = (Button)findViewById(R.id.btnEnviar);
        rdbSi = (RadioButton)findViewById(R.id.rdbSi);
        rdbNo = (RadioButton)findViewById(R.id.rdbNo);
        wsConsultarCursos();

        Resources res = getResources();

        tabs=(TabHost)findViewById(android.R.id.tabhost);
        tabs.setup();

        TabHost.TabSpec spec=tabs.newTabSpec("mitab1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Curso Activo");
        tabs.addTab(spec);

        spec=tabs.newTabSpec("mitab2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Lista Cursos");
        tabs.addTab(spec);

        tabs.setCurrentTab(0);

        tabs.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String s) {
                if (s == "mitab1")
                {
                    if (id!=0)
                    {
//                        wsConsultarCursoActi();
//                        Toast.makeText(Activity_Cursos.this,"su id es "+s, Toast.LENGTH_LONG).show();
                    }else
                    {

                    }
                }

                //aqui muestro un mensaje para ver si si funciona y si me dice que tab es
            }
        });


        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(id != 0){
                    if(rdbNo.isChecked()){
                        wsConfirmarAsistencia(0);
                    }else if(rdbSi.isChecked()){
                        wsConfirmarAsistencia(1);
                    }
                }else
                    Toast.makeText(Activity_Cursos.this,"Selecciona curso", Toast.LENGTH_SHORT).show();
            }
        });


    }


    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) { // esta parte es el que llena la lista

        Curso curso = null;
        final ArrayList<Curso> listado = new ArrayList<>();
        JSONArray json = response.optJSONArray("estados");
        try {
            for (int i=0;i<json.length();i++){
                Curso c  = new Curso();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                c.setNombre(jsonObject.optString("nomCurso"));
                c.setDonde(jsonObject.optString("nombreUni"));
                c.setId(jsonObject.optInt("idCursos"));
                listado.add(c);
                adapter = new Activity_Cursos.MyArrayAdapter(Activity_Cursos.this,R.layout.layout_curso,listado);
                listaCursos.setAdapter(adapter);

                listaCursos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Curso cc = listado.get(i);
                        id = cc.getId(); // aqui jalo el id cuando le doy click al item o curso
//                        Toast.makeText(Activity_Cursos.this,"su id es "+id, Toast.LENGTH_LONG).show();
                        wsConsultarCursoActi();

                    }
                });
            }
        }catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    class MyArrayAdapter extends ArrayAdapter<Curso> {  // el adapter para la lista
        Context context;
        int textViewRecursoId;
        ArrayList<Curso> objects;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Curso> objects) {
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewRecursoId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View convertView, ViewGroup viewGroup) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewRecursoId, null);

            TextView lblNombre = (TextView) view.findViewById(R.id.txtNombre);
            TextView lblTelefono = (TextView) view.findViewById(R.id.txtNombre2);

            lblNombre.setText(objects.get(position).getNombre());
            lblTelefono.setText(objects.get(position).getDonde());
            return view;
        }

    }

    public void wsConsultarCursoActi()
    {
        String url = serverip + "wsConsultarCursoActivo.php?idCurso=" + id;
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray json = response.optJSONArray("cursos");
                        try
                        {
                            JSONObject jsonObject = null;
                            jsonObject = json.getJSONObject(0);
                            txtNombre.setText(jsonObject.optString("nomCurso"));
                            txtImpartida.setText(jsonObject.optString("nombreUni"));
                            txtDia.setText(jsonObject.optString("fechaCurso"));
                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        request.add(jsonObjectRequest);
        tabs.setCurrentTab(0);
    }


    public void wsConfirmarAsistencia( Integer asistencia)
    {
        String url = serverip + "wsConfirmarAsistencia.php?idUsuario=" + usuario.getId()
                + "&idCurso=" + id + "&asistencia=" + asistencia;
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray json = response.optJSONArray("resultado");
                        try {
                            JSONObject jsonObject = null;
                            jsonObject = json.getJSONObject(0);
                            enviado = jsonObject.optString("enviado");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(Activity_Cursos.this,enviado,Toast.LENGTH_SHORT).show();
                        Log.wtf("PINTA",enviado);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        request.add(jsonObjectRequest);
    }



    public void wsConsultarCursos() { // para consultar los cursos
        String url = serverip + "wsConsultarCursos.php";
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

}
