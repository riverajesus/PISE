package com.example.pise;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import DB.ProcesosPHP;

public class tab_Posgrado extends Fragment implements Response.Listener<JSONObject>, Response.ErrorListener {

    private TextView lblNombrePos;
    private EditText edtPosgrado;
    private EditText edtEscuela;
    private RadioButton rdbSi;
    private RadioButton rdbNo;
    private Button btnInsertPos;
    private RadioGroup rgPregunta;

    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private String serverip = "https://pise-website-1.000webhostapp.com/WebServices/";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tab_posgrado, container, false);

        request = Volley.newRequestQueue(view.getContext());
        lblNombrePos = (TextView) view.findViewById(R.id.lblNombrePos);
        edtPosgrado = (EditText) view.findViewById(R.id.edtPosgrado);
        edtEscuela = (EditText) view.findViewById(R.id.edtEscuela);
        rdbSi = (RadioButton) view.findViewById(R.id.rdbSi);
        rdbNo = (RadioButton) view.findViewById(R.id.rdbNo);
        btnInsertPos = (Button) view.findViewById(R.id.btnInsertPos);
        rgPregunta = (RadioGroup) view.findViewById(R.id.rgPregunta);

        final int id = getArguments().getInt("id");
        final ProcesosPHP procesosPHP = new ProcesosPHP();
        procesosPHP.setContext(view.getContext());
        final Usuario user = new Usuario();
        user.setId(id);
        wsConsultarPosgrado(user);

        btnInsertPos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtEscuela.getText().toString().trim().isEmpty() || edtPosgrado.getText().toString().trim().isEmpty() &&
                        (rdbSi.isChecked() || rdbNo.isChecked())){
                    Toast.makeText(view.getContext(), "Todos los campos deben estar llenos y sin espacios en blanco", Toast.LENGTH_SHORT).show();
                }else {
                    user.setNombrePosgrado(edtPosgrado.getText().toString());
                    user.setNombreEscuelaPosgrado(edtEscuela.getText().toString());
                    if (rdbSi.isChecked()){
                        user.setDeseoPosgrado(1);
                    }else if (rdbNo.isChecked()){
                        user.setDeseoPosgrado(0);
                    }
                    procesosPHP.wsModificarPosgrado(user);
                    Toast.makeText(view.getContext(), "Datos modificados", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {

        Usuario usuario = null;
        final JSONArray json = response.optJSONArray("Trabajos");
        try {
            for (int i=0;i<json.length();i++){
                usuario = new Usuario();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                edtPosgrado.setText(jsonObject.optString("nomPosgrado"));
                edtEscuela.setText(jsonObject.optString("escuela"));
                lblNombrePos.setText(jsonObject.optString("nombre")+" "+jsonObject.optString("p_paterno")+" "+jsonObject.optString("p_materno"));
                //prueba.setText(jsonObject.optString("deseo"));

                if(jsonObject.optString("deseo").equals("1")){
                    rgPregunta.check(rdbSi.getId());
                }else {
                    rgPregunta.check(rdbNo.getId());
                }
            }

        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    public void wsConsultarPosgrado(Usuario c) {
        String url = serverip + "wsConsultarPosgrado.php?idUsuario=" + c.getId();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }
}
