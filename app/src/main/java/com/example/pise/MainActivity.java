package com.example.pise;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import DB.ProcesosPHP;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener {
    private EditText edtUser;
    private EditText edtContra;
    private Button btnIngresar;
    private Button btnRecuperar;
    private Usuario user;
    private JsonObjectRequest jsonObjectRequest;
    private RequestQueue request;
    private final Context context = MainActivity.this;
    ProcesosPHP php;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtUser = (EditText) findViewById(R.id.edtUser);
        edtContra = (EditText) findViewById(R.id.edtPass);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnRecuperar = (Button) findViewById(R.id.btnRecpass);

        php = new ProcesosPHP();
        php.setContext(context);
        request = Volley.newRequestQueue(context);
        user = new Usuario();


        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    if (!edtContra.getText().toString().trim().isEmpty() && !edtUser.getText().toString().trim().isEmpty()) {
                        wsValidarLogin();
                    } else {
                        Toast.makeText(MainActivity.this, "Para ingresar son requeridos Contraseña y Usuario", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Es necesario conexión a internet", Toast.LENGTH_SHORT).show();

                }


            }
        });
        btnRecuperar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    if(!edtUser.getText().toString().trim().equals(""))
                        wsRecuperarContraseña();
                    else
                        Toast.makeText(MainActivity.this, "Es necesario capturar usuario", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Es necesario conexión a internet", Toast.LENGTH_SHORT).show();

                }

            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    public void wsValidarLogin() {
        try {
            String url = php.getServerip() + "wsValidarLogin.php?user=" + edtUser.getText().toString().trim()
                    + "&pass=" + edtContra.getText().toString().trim();
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            request.add(jsonObjectRequest);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al intentar", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onResponse(JSONObject response) {
        user = new Usuario();
        JSONArray json = response.optJSONArray("usuarios");
        try {
            JSONObject jsonObject = null;
            jsonObject = json.getJSONObject(0);
            user.setId(jsonObject.optInt("idusuario"));
            user.setNombre(jsonObject.optString("usuario"));
            user.setContraseña(jsonObject.optString("password"));
            Intent i = new Intent(MainActivity.this,Activity_Menu.class);
            Bundle Object= new Bundle();
            Object.putSerializable("usuario",user);
            i.putExtras(Object);
            startActivity(i);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "El Usuario o Contraseña son incorrectos", Toast.LENGTH_LONG).show();
    }

    public void wsRecuperarContraseña() {
        try {
            String url = php.getServerip() + "wsRecuperarContraseña.php?user=" + edtUser.getText().toString();
            url = url.replace(" ", "%20");
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,

            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    JSONArray json = response.optJSONArray("resultado");
                    try {
                        JSONObject jsonObject = null;
                        jsonObject = json.getJSONObject(0);
                        String imp = (jsonObject.optString("correo"));
                        if(imp.length() > 0)
                            Toast.makeText(MainActivity.this, "Revisa tu correo", Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(MainActivity.this, "Usuario no existente", Toast.LENGTH_LONG).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), "Usuario no existente", Toast.LENGTH_LONG).show();

                }
            });

            request.add(jsonObjectRequest);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error al enviar", Toast.LENGTH_LONG).show();
        }


    }
}
