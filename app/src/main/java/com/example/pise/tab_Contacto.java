package com.example.pise;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import DB.ProcesosPHP;

public class tab_Contacto extends Fragment implements Response.Listener<JSONObject>,Response.ErrorListener{
    private EditText txtTelefono;
    private EditText txtMovil;
    private EditText txtCorreo;
    private EditText txtCorreoAlterno;
    private Button btnActualizar;
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private JsonArrayRequest jsonArrayRequest;
    private String serverip = "https://pise-website-1.000webhostapp.com/WebServices/";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.tab_contacto, container, false);



        int id = getArguments().getInt("id");

        //Toast.makeText(view.getContext(), ""+id, Toast.LENGTH_SHORT).show();

        request = Volley.newRequestQueue(view.getContext());
        txtTelefono = (EditText) view.findViewById(R.id.txtTelefono);
        txtMovil = (EditText) view.findViewById(R.id.txtMovil);
        txtCorreo = (EditText) view.findViewById(R.id.txtCorreo);
        txtCorreoAlterno = (EditText) view.findViewById(R.id.txtCorreoAlterno);
        btnActualizar = (Button) view.findViewById(R.id.btnActualizar);

        final ProcesosPHP procesosPHP = new ProcesosPHP();

        procesosPHP.setContext(view.getContext());

        final Usuario user = new Usuario();

        user.setId(id);

        wsConsultarContacto(user);

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txtTelefono.getText().toString().trim().isEmpty() || txtMovil.getText().toString().trim().isEmpty() ||
                    txtCorreo.getText().toString().trim().isEmpty() || txtCorreoAlterno.getText().toString().trim().isEmpty()){
                    Toast.makeText(view.getContext(), "Todos los campos deben estar llenos y sin espacios en blanco", Toast.LENGTH_SHORT).show();
                }else {
                    //Toast.makeText(view.getContext(), "si jalo", Toast.LENGTH_SHORT).show();

                    user.setTelefono(txtTelefono.getText().toString());
                    user.setCelular(txtMovil.getText().toString());
                    user.setEmail(txtCorreo.getText().toString());
                    user.setEmailAlterno(txtCorreoAlterno.getText().toString());

                    procesosPHP.wsModificarContacto(user);
                    Toast.makeText(view.getContext(), "Datos modificados", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(final JSONObject response) {

        Usuario usuario = null;
        final JSONArray json = response.optJSONArray("usuarios");

        try {
            for (int i=0;i<json.length();i++){
                usuario = new Usuario();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                txtTelefono.setText(jsonObject.optString("telefono"));
                txtMovil.setText(jsonObject.optString("celular"));
                txtCorreo.setText(jsonObject.optString("email"));
                txtCorreoAlterno.setText(jsonObject.optString("email_alterno"));
            }

        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    public void wsConsultarContacto(Usuario c) {
        String url = serverip + "wsConsultarContacto.php?idusuario=" + c.getId();
        //url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }
}
