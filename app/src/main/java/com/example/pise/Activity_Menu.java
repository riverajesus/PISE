package com.example.pise;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import DB.ProcesosPHP;

public class Activity_Menu extends AppCompatActivity implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener {

    private ImageButton btnCerrar;
    private ImageButton btnPerfil;
    private ImageButton btnActividad;
    private ImageButton btnCursos;
    private ImageButton btnVacantes;
    private Intent i;
    private Bundle bundle;
    private Usuario usuario;
    ProcesosPHP php;
    private final Context context = this;
    private JsonObjectRequest jsonObjectRequest;
    private RequestQueue request;
    private Boolean llenado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        btnCerrar = (ImageButton) findViewById(R.id.btnCerrarSesion);
        btnPerfil = (ImageButton) findViewById(R.id.btnPerfil);
        btnActividad = (ImageButton) findViewById(R.id.btnActividad);
        btnCursos = (ImageButton) findViewById(R.id.btnCursos);
        btnVacantes = (ImageButton) findViewById(R.id.btnVacantes);

        i = this.getIntent();
        bundle = i.getExtras();
        usuario = new Usuario();
        usuario = (Usuario) bundle.getSerializable("usuario");
        php = new ProcesosPHP();
        php.setContext(context);
        request = Volley.newRequestQueue(context);
        llenado=false;
        informacionPerfil();

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Activity_Menu.this, MainActivity.class);
                startActivity(intent);
            }
        });

        btnPerfil.setOnClickListener(new View.OnClickListener() {///me cierra la app
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Activity_Menu.this, Activity_Perfil.class);
                Bundle Object= new Bundle();
                Object.putSerializable("usuario",usuario);
                intent.putExtras(Object);
                startActivity(intent);
            }
        });

        btnActividad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Activity_Menu.this, Activity_Actividad_actual.class);
                Bundle Object= new Bundle();
                Object.putSerializable("usuario",usuario);
                intent.putExtras(Object);
                startActivity(intent);
            }
        });

        btnCursos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Activity_Menu.this, Activity_Cursos.class);
                Bundle Object= new Bundle();
                Object.putSerializable("usuario",usuario);
                intent.putExtras(Object);
                startActivity(intent);
            }
        });

        btnVacantes.setOnClickListener(new View.OnClickListener() {//me cierra la app
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Activity_Menu.this, Activity_Vacantes.class);
                startActivity(intent);
            }
        });
    }

    public void informacionPerfil() {
        try {
            String url = php.getServerip() + "wsConsultarPerfil.php?idusuario=" + usuario.getId();
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            JSONArray json = response.optJSONArray("usuarios");
                            try {
                                JSONObject jsonObject = null;
                                jsonObject = json.getJSONObject(0);
                                usuario.setCarrera(jsonObject.optString("carrera"));
                                usuario.setFechaNac(jsonObject.optString("fecha_nacimiento"));
                                usuario.setFotoUrl(jsonObject.optString("fotoUrl"));
                                usuario.setNombre(jsonObject.optString("nombre"));
                                usuario.setSexo(jsonObject.optString("sexo"));
                                informacionContacto();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "Ha ocurrido un fallo en la conexion a internet 1", Toast.LENGTH_LONG).show();
                        }
                    });
            request.add(jsonObjectRequest);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "No se han al Usuario 2", Toast.LENGTH_LONG).show();
        }
    }
    public void informacionContacto() {
        try {
            String url = php.getServerip() + "wsConsultarContacto.php?idusuario=" + usuario.getId();
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            JSONArray json = response.optJSONArray("usuarios");
                            try {
                                JSONObject jsonObject = null;
                                jsonObject = json.getJSONObject(0);
                                usuario.setCelular(jsonObject.optString("celular"));
                                usuario.setEmail(jsonObject.optString("email"));
                                usuario.setEmailAlterno(jsonObject.optString("email_alterno"));
                                usuario.setTelefono(jsonObject.optString("telefono"));
                                informacionPostGrado();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "Ha ocurrido un fallo en la conexion a internet 2", Toast.LENGTH_LONG).show();
                        }
                    });
            request.add(jsonObjectRequest);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "No se han al Usuario 2", Toast.LENGTH_LONG).show();
        }
    }
    public void informacionPostGrado() {
        try {
            String url = php.getServerip() + "wsConsultarPosgrado.php?idUsuario=" + usuario.getId();
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            JSONArray json = response.optJSONArray("Trabajos");
                            try {
                                JSONObject jsonObject = null;
                                jsonObject = json.getJSONObject(0);
                                usuario.setDeseoPosgrado(jsonObject.optInt("deseo"));
                                usuario.setNombreEscuelaPosgrado(jsonObject.optString("escuela"));
                                usuario.setNombrePosgrado(jsonObject.optString("nombre"));
                                informacionLaboral();
                            } catch (JSONException e) {
                                Log.d("mal",e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "Ha ocurrido un fallo en la conexion a internet 3", Toast.LENGTH_LONG).show();
                        }
                    });
            request.add(jsonObjectRequest);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "No se han al Usuario 3", Toast.LENGTH_LONG).show();
        }
    }
    public void informacionLaboral() {
        try {
            String url = php.getServerip() + "wsConsultarLaboral.php?idUsuario=" + usuario.getId();
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            JSONArray json = response.optJSONArray("Trabajos");
                            try {
                                JSONObject jsonObject = null;
                                jsonObject = json.getJSONObject(0);
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                    usuario.setFecInicio(jsonObject.optString("fecha_inicio"));
                                usuario.setTrabajo(jsonObject.optString("nombre_trabajo"));
                                usuario.setPuesto(jsonObject.optString("puesto"));
                                //Toast.makeText(getApplicationContext(),"listo", Toast.LENGTH_LONG).show();
                               // Log.d("usuario llenado","Id: "+usuario.getId()+"Nombre: "+usuario.getNombre()+"Contraseña: "+usuario.getContraseña()+"Tel: "+usuario.getTelefono()+"Cel: "+usuario.getCelular()+"Email : "+usuario.getEmail()+"Email2: "+usuario.getEmailAlterno()+"Carrera: "+usuario.getCarrera()+"foto: "+usuario.getFotoUrl()+"fechanac: "+usuario.getFechaNac()+"sex: "+usuario.getSexo()+"job: "+usuario.getTrabajo()+"puesto: "+usuario.getPuesto()+"fechini: "+usuario.getFecInicio()+"Nombre escu: "+usuario.getNombreEscuelaPosgrado()+"post: "+usuario.getNombrePosgrado()+"desde: "+usuario.getDeseoPosgrado());
                                llenado=true;
                            } catch (JSONException e) {
                                Log.d("mal",e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), "Ha ocurrido un fallo en la conexion a internet 4", Toast.LENGTH_LONG).show();
                        }
                    });
            request.add(jsonObjectRequest);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "No se han al Usuario 4", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "Ha ocurrido un fallo en la conexion a internet", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(JSONObject response) {


    }
}
