package com.example.pise;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import DB.ProcesosPHP;

public class tab_MiPerfil extends Fragment implements Response.Listener<JSONObject>,Response.ErrorListener{
    private TextView lblNombre;
    private TextView lblCarrera;
    private TextView lblFechaNacimiento;
    private TextView lblSexo;
    private ImageView imageView;
    private RequestQueue request;
    private String urlImgane="";
    private JsonObjectRequest jsonObjectRequest;
    private JsonArrayRequest jsonArrayRequest;
    private final tab_MiPerfil context = this;
    private String serverip = "https://pise-website-1.000webhostapp.com/WebServices/";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_mi_perfil, container, false);


        int id = getArguments().getInt("id");

        //Toast.makeText(view.getContext(), ""+id, Toast.LENGTH_SHORT).show();



        request = Volley.newRequestQueue(view.getContext());
        imageView = (ImageView) view.findViewById(R.id.imageView3);
        lblNombre = (TextView) view.findViewById(R.id.lblNombre);
        lblCarrera = (TextView) view.findViewById(R.id.lblCarrera);
        lblFechaNacimiento = (TextView) view.findViewById(R.id.lblFechaNacimiento);
        lblSexo = (TextView) view.findViewById(R.id.lblSexo);

        ProcesosPHP procesosPHP = new ProcesosPHP();

        Usuario user = new Usuario();



        user.setId(id);

        wsConsultarPerfil(user);

        //Log.wtf("url antes de llamar",""+urlImgane);


        return view;
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(final JSONObject response) {
        Log.wtf("carrera","ENTRA PERRO");
        Usuario usuario = null;
        final JSONArray json = response.optJSONArray("usuarios");
        Log.wtf("carrera",""+json.length());
        try {
            for (int i=0;i<json.length();i++){
                usuario = new Usuario();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                //usuario.setCarrera(jsonObject.optString("carrera"));
                //Log.wtf("carrera",""+jsonObject.optString("carrera"));
                lblCarrera.setText(jsonObject.optString("carrera"));
                lblNombre.setText(jsonObject.optString("nombre"));
                lblFechaNacimiento.setText(jsonObject.optString("fecha_nacimiento"));
                lblSexo.setText(jsonObject.optString("sexo"));
                urlImgane=jsonObject.optString("fotoUrl");
                Log.wtf("url",""+jsonObject.optString("fotoUrl"));
                if (urlImgane.isEmpty()){
                    imageView.setImageResource(R.drawable.user_nav);
                }else {
                    Picasso.with(getContext()).load(urlImgane).into(imageView);
                }
            }

        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    public void wsConsultarPerfil(Usuario c) {
        String url = serverip + "wsConsultarPerfil.php?idusuario=" + c.getId();
        //url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        Log.wtf("TEST",""+jsonObjectRequest);
        request.add(jsonObjectRequest);
    }
}
