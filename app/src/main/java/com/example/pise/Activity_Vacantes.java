package com.example.pise;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTabHost;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Activity_Vacantes extends FragmentActivity implements View.OnClickListener, Response.Listener<JSONObject>, Response.ErrorListener{

    ListView lista;
    ArrayList<Vacante> lista_Vacantes;
    private JsonObjectRequest jsonObjectRequest;
    private Activity_Vacantes.MyArrayAdapter adapter;
    private RequestQueue request;
    private String serverip = "https://pise-website-1.000webhostapp.com/WebServices/";

    TextView txtEmpresa;
    TextView txtPuesto;
    EditText edtRequisitos;

    TabHost tabs;

    public int id=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacantes);
        lista = findViewById(R.id.lvVacantes);
        request = Volley.newRequestQueue(Activity_Vacantes.this);
        txtPuesto = (TextView) findViewById(R.id.txtPuesto);
        txtEmpresa = (TextView) findViewById(R.id.txtEmpresa);
        edtRequisitos = (EditText) findViewById(R.id.edtRequisitos);
        lista_Vacantes = new ArrayList<Vacante>();
        //mandar llamar consultar curso
        wsConsultarVacantes();

        tabs=(TabHost)findViewById(android.R.id.tabhost);
        tabs.setup();

        TabHost.TabSpec spec=tabs.newTabSpec("mitab1");
        spec.setContent(R.id.tab1);
        spec.setIndicator("Lista Vacantes");
        tabs.addTab(spec);

        spec=tabs.newTabSpec("mitab2");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Vacante");
        tabs.addTab(spec);

        tabs.setCurrentTab(0);

    }


    @Override
    public void onClick(View view) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {
        Vacante vac = null;
        JSONArray json = response.optJSONArray("vacantes");
        try {
            for(int i=0;i<json.length();i++){
                vac = new Vacante();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                vac.setId(jsonObject.optInt("id"));
                vac.setNombre(jsonObject.optString("nombre"));
                vac.setDonde(jsonObject.optString("nombre_empresa"));
                lista_Vacantes.add(vac);
            }
            adapter = new Activity_Vacantes.MyArrayAdapter(Activity_Vacantes.this,R.layout.layout_vacante,lista_Vacantes);
            lista.setAdapter(adapter);

            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    //Toast.makeText(Activity_Vacantes.this,"hiciste clicken: " + lista_Vacantes.get(i).getId(),Toast.LENGTH_SHORT).show();
                    id = lista_Vacantes.get(i).getId();
                    wsConsultarVacante();

                }
            });

        } catch (JSONException e) {
            //Toast.makeText(getContext(),"Valio verga",Toast.LENGTH_LONG).show();
            e.printStackTrace();

        }
    }

    class MyArrayAdapter extends ArrayAdapter<Vacante> {
        Context context;
        int textViewRecursoId;
        ArrayList<Vacante> objects;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Vacante> objects) {
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewRecursoId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View convertView, ViewGroup viewGroup) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewRecursoId, null);

            TextView lblNombre = (TextView) view.findViewById(R.id.txtNombre);
            TextView lblTelefono = (TextView) view.findViewById(R.id.txtEmpresa);

            lblNombre.setText(objects.get(position).getNombre());
            lblTelefono.setText(objects.get(position).getDonde());

            return view;

        }

    }
    public void wsConsultarVacante() {
        String url = serverip + "wsConsultarVacante.php?idVacante=" + id;
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONArray json = response.optJSONArray("vacantes");
                try {
                    for (int i=0;i<json.length();i++){
                        JSONObject jsonObject = null;
                        jsonObject = json.getJSONObject(i);
                        txtPuesto.setText(jsonObject.optString("puesto"));
                        txtEmpresa.setText(jsonObject.optString("nombre_empresa"));
                        edtRequisitos.setText(jsonObject.optString("requisitos"));
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        request.add(jsonObjectRequest);
        tabs.setCurrentTab(1);
    }

    public void wsConsultarVacantes() {
        String url = serverip + "wsConsultarVacantes.php";
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

}





