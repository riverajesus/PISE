package com.example.pise;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class tab_CursoActivo extends Fragment {

    private TextView txtNombre;
    private TextView txtImpartida;
    private TextView txtDia;
    private Button btnEnviar;
    private RadioButton rdbSi;
    private RadioButton rdbNo;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_curso_activo, container, false);

        txtNombre = (TextView) view.findViewById(R.id.txtNombre);
        txtImpartida = (TextView) view.findViewById(R.id.txtImpartida);
        txtDia = (TextView) view.findViewById(R.id.txtDia);
        btnEnviar = (Button)view.findViewById(R.id.btnEnviar);
        rdbSi = (RadioButton)view.findViewById(R.id.rdbSi);
        rdbNo = (RadioButton)view.findViewById(R.id.rdbNo);

        return view;


    }

}
