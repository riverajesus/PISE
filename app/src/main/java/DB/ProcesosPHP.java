package DB;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.Usuario;
import com.example.pise.Vacante;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProcesosPHP implements Response.Listener<JSONObject>, Response.ErrorListener {
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;

    private JsonArrayRequest jsonArrayRequest;
    private String serverip = "https://pise-website-1.000webhostapp.com/WebServices/";

    public void setContext(Context context) {
        request = Volley.newRequestQueue(context);
    }

    public void wsConfirmarAsistencia(Usuario c, int idCurso) {
        String url = getServerip() + "wsConfirmarAsistencia.php?idUsuario=" + c.getId()
                            + "&idCurso=" + idCurso;
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsConsultarContacto(Usuario c) {
        String url = getServerip() + "wsConsultarContacto.php?idusuario=" + c.getId();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsConsultarCursoActivo(int idCurso) {
        String url = getServerip() + "wsConsultarCursoActivo.php?idCurso=" + idCurso;
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsConsultarCursos() {
        String url = getServerip() + "wsConsultarCursos.php";
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsConsultarLaboral(Usuario c) {
        String url = getServerip() + "wsConsultarLaboral.php?idUsuario=" + c.getId();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsConsultarPerfil(Usuario c) {
        String url = getServerip() + "wsConsultarPerfil.php?idusuario=" + c.getId();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsConsultarPosgrado(Usuario c) {
        String url = getServerip() + "wsConsultarPosgrado.php?idUsuario=" + c.getId();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsConsultarVacante(Vacante c) {
        String url = getServerip() + "wsConsultarVacante.php?idVacante=" + c.getId();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsConsultarVacantes() {
        String url = getServerip() + "wsConsultarVacantes.php";
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsModificarContacto(Usuario c) {
        String url = getServerip() + "wsModificarContacto.php?idUsuario=" + c.getId()
                + "&telefono=" + c.getTelefono()
                + "&celular=" + c.getCelular()
                + "&email=" + c.getEmail()
                + "&emailAlterno=" + c.getEmailAlterno();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsModificarLaboral(Usuario c) {
        String url = getServerip() + "wsModificarLaboral.php?idUsuario=" + c.getId()
                + "&trabajo=" + c.getTrabajo()
                + "&puesto=" + c.getPuesto()
                + "&fecha_inicio=" + c.getFecInicio();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsModificarPosgrado(Usuario c) {
        String url = getServerip() + "wsModificarPosgrado.php?idUsuario=" + c.getId()
                + "&nombre=" + c.getNombrePosgrado()
                + "&escuela=" + c.getNombreEscuelaPosgrado()
                + "&deseo=" + c.getDeseoPosgrado();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsValidarLogin(Usuario c) {
        String url = getServerip() + "wsValidarLogin.php?user=" + c.getNombre()
                + "&pass=" + c.getContraseña();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsRecuperarContraseña(Usuario c) {
        String url = getServerip() + "wsRecuperarContraseña.php?user=" + c.getNombre();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR", error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {

    }

    public String getServerip() {
        return serverip;
    }

    public void setServerip(String serverip) {
        this.serverip = serverip;
    }
}